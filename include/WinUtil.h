#pragma once
#include <chrono>
#include <cstdlib>
#include <fstream>
#include <random>
#include <string>
#include <tuple>

#include <windows.h>

namespace winutil {
extern "C" NTSTATUS NTAPI RtlAdjustPrivilege(ULONG, BOOLEAN, BOOLEAN, PBOOLEAN);
extern "C" NTSTATUS NTAPI NtSetInformationProcess(HANDLE, ULONG, PVOID, ULONG);

std::tuple<int, int> GetResolution();
void SetCritical();
int StartHidden(std::string path, std::string args);

// Class that if initialized will create a mutex to ensure a single run for an application. Put at the top of main() or WinMain().
class RunOnce {
    HANDLE hMutex{nullptr};

public:
    RunOnce(const char * identifier) {
        hMutex = ::OpenMutex(MUTEX_ALL_ACCESS, -1, identifier);

        if(!hMutex)
            hMutex = ::CreateMutex(0, 0, identifier);
        else
            std::exit(0);
    }

    ~RunOnce() {
        ::ReleaseMutex(hMutex);
    }

    RunOnce() = delete;
    RunOnce(RunOnce& other) = delete;
    RunOnce(RunOnce&& other) = delete;
    RunOnce& operator=(RunOnce const& other) = delete;
};

int randInt(int max);

} // namespace winutil
