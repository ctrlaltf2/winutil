#pragma once
#include <iostream>

#include <windows.h>
#include <shlobj.h>

#include "WinUtil.h"

namespace winutil {
    namespace persist {
        bool Registry(std::string, std::string);
        // bool WMI(std::string, std::string);
        bool File(std::string);
    }
}
